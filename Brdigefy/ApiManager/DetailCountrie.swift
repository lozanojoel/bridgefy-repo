//
//  DetailCountrie.swift
//  Brdigefy
//
//  Created by Joel Lozano on 03/05/21.
//

import Foundation

struct DetailViewCountrie: Decodable{
    var name: String?
    var topLevelDomain: [String]?
    var alpha2Code: String?
    var alpha3Code: String?
    var callingCodes: [String?]
    var capital: String?
    var altSpellings: [String?]
    var region: String
    var subregion: String
    var population: Int
    var latlng: [Int?]
    var demonym: String
    var area: Int
    var gini: Double
    var timezones: [String]
    var borders: [String]
    var nativeName: String
    var numericCode: String
    var currencies: [String]
    var languages: [String]?
}

struct ContriesDetailRequest {
    var urlRequest: URLRequest
    let headers = [
        "x-rapidapi-key": "c220045095msh0e500381f036195p1a352djsnb2b4a91b96e3",
        "x-rapidapi-host": "restcountries-v1.p.rapidapi.com"
    ]
    
    init(countriCode: String) {
        let resouceString = "https://restcountries-v1.p.rapidapi.com/alpha/\(countriCode)"
        guard let resourceURL = URL(string: resouceString) else { fatalError()}
        let resouceURL = resourceURL
        
        urlRequest = URLRequest(url: resouceURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        urlRequest.allHTTPHeaderFields = headers
        urlRequest.httpMethod = "GET"
        
    }
    
    func getDetailCountrie(completion: @escaping(Result<DetailViewCountrie, CountriesError>) -> Void) {
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, URLResponse, CountriesError) in
                guard let jsonData = data else {
                    completion(.failure(.noDataAvialable))
                    return
                }
                do {
                    let countrieResponse = try JSONDecoder().decode(DetailViewCountrie.self, from: jsonData)
                    let countrieDetail = countrieResponse
                    completion(.success(countrieDetail))
                } catch {
                    completion(.failure(.canNotProcessData))
                }
        }
        dataTask.resume()
    }
}

