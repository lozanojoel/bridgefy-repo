//
//  Countries.swift
//  Brdigefy
//
//  Created by Joel Lozano on 01/05/21.
//

import Foundation

enum CountriesError: Error {
    case noDataAvialable
    case canNotProcessData
}

struct ContriesRequest {
    var urlRequest: URLRequest
    let headers = [
        "x-rapidapi-key": "c220045095msh0e500381f036195p1a352djsnb2b4a91b96e3",
        "x-rapidapi-host": "restcountries-v1.p.rapidapi.com"
    ]
    
    init() {
        let resouceString = "https://restcountries-v1.p.rapidapi.com/all"
        guard let resourceURL = URL(string: resouceString) else { fatalError()}
        let resouceURL = resourceURL
        
        urlRequest = URLRequest(url: resouceURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        urlRequest.allHTTPHeaderFields = headers
        urlRequest.httpMethod = "GET"
    }
    
    func getCountries(completion: @escaping(Result<[DetailCountry], CountriesError>) -> Void) {
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, URLResponse, CountriesError) in
                guard let jsonData = data else {
                    completion(.failure(.noDataAvialable))
                    return
                }
                
                do {
                    let countriesResponse = try JSONDecoder().decode([DetailCountry].self, from: jsonData)
                    let countriesDetails = countriesResponse
                    completion(.success(countriesDetails))
                } catch {
                    completion(.failure(.canNotProcessData))
                }
            
        }
        dataTask.resume()
    }
}

struct CountriesResponse: Decodable {
    let reponse: [DetailCountry]
}

struct DetailCountry: Decodable {
    let name: String
    let alpha2Code: String
    let alpha3Code: String
    let region: String
}
