//
//  File.swift
//  Brdigefy
//
//  Created by Joel Lozano on 01/05/21.
//

import Foundation
import UIKit

protocol LoginWireframeProtocol {
    func showTableView(baseController: UIViewController)
    func showError(baseController: UIViewController)
}

class LoginWireframe: LoginWireframeProtocol {
    
    func showTableView(baseController: UIViewController) {
       let module = CountriesModule(baseController: baseController)
        module.showTabBar()
    }
    
    func showError(baseController: UIViewController) {
       let errorModule = ErrorModule(baseController: baseController)
        errorModule.showGenericError(message: "Error en tus credenciales, comprueba tu mail y password")
    }
}
