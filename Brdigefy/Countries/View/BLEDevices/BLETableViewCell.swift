//
//  BLETableViewCell.swift
//  Brdigefy
//
//  Created by Joel Lozano on 03/05/21.
//

import UIKit

class BLETableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var identifier: UILabel!
    @IBOutlet weak var status: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
