//
//  ErrorViewController.swift
//  Brdigefy
//
//  Created by Joel Lozano on 05/05/21.
//

import UIKit

class ErrorViewController: UIViewController {

    @IBOutlet weak var buttonAction: UIButton! {
        didSet {
            buttonAction.setTitle("Continuar", for: .normal)
            buttonAction.backgroundColor = UIColor.orange
            buttonAction.titleLabel?.tintColor = UIColor.white
        }
    }
    @IBOutlet weak var titleModal: UILabel! {
        didSet {
            titleModal.text = "Error"
        }
    }
    @IBOutlet weak var messageError: UILabel! {
        didSet {
            messageError.text = ""
        }
    }
    
    var presenter: ErrorPresenterProtocol
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor.clear
        setup(message: message)
    }
    
    var message: String

    init(message: String,presenter: ErrorPresenterProtocol) {
        self.presenter = presenter
        self.message = message
        super.init(nibName: "ErrorViewController", bundle: nil)
        
    }
    
    func setup(message: String) {
        messageError.text = message
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func dissmissModal(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
}
