//
//  LoginManager.swift
//  Brdigefy
//
//  Created by Joel Lozano on 02/05/21.
//

import Foundation
import UIKit


class LoginManager {
    let modelUser = """
        {
        "email": "challenge@bridgefy.me",
        "password": "P@$$w0rD!"
    }
    """
    
    func getUserValidation() -> [String: Any]{
        let jsonUser = Data(modelUser.utf8)
        do {
            if let json = try JSONSerialization.jsonObject(with: jsonUser, options: []) as? [String: Any] {
                return json
            }
        } catch {
            return ["error": "Error en tus credenciales"]
        }
        return ["":""]
    }
}
