//
//  ErrorModule.swift
//  Brdigefy
//
//  Created by Joel Lozano on 04/05/21.
//

import Foundation
import UIKit


class ErrorModule {
    
    var presenter: ErrorPresenterProtocol
    var baseController: UIViewController
    
    init(baseController: UIViewController) {
        self.presenter = ErrorPresenter()
        self.baseController = baseController
    }
    
    func showGenericError(message: String) {
        presenter.showErrorMessage(message: message, baseController: baseController)
    }
}
