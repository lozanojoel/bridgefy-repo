//
//  LoginInteractor.swift
//  Brdigefy
//
//  Created by Joel Lozano on 01/05/21.
//

import Foundation


enum LoginStatus {
    case success
    case fail
}

protocol LoginInteractorProtocol {
    func login(email: String, password: String) -> LoginStatus
}

class LoginInteractor: LoginInteractorProtocol {
    
    func login(email: String, password: String) -> LoginStatus {
        if (validateEmail(email: email)){
             let user = APIManager.API.loginValidation()
            if (user["email"] as! String == email && user["password"] as! String == password) {
                return LoginStatus.success
            } else {
                return LoginStatus.fail
            }
        } else {
            return LoginStatus.fail
        }
    }
    
    func validateEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailPred.evaluate(with: email)
    }
}
