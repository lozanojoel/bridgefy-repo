//
//  CountriesWireframe.swift
//  Brdigefy
//
//  Created by Joel Lozano on 02/05/21.
//

import Foundation
import UIKit

protocol CountriesWireframeProtocol {
    func showTabBar(baseController: UIViewController, presenter: CountriesPresenterProtocol)
    func showDetailCountrie(date: DetailCountry, basecontroller: UIViewController)
}

class CountriesWireframe: CountriesWireframeProtocol {
    
    var baseController: UIViewController
    var navigationController: UINavigationController? {
        return (baseController as? UINavigationController) ?? baseController.navigationController
    }
    
    init(baseController: UIViewController) {
        self.baseController = baseController
    }
    
    
    func showTabBar(baseController: UIViewController, presenter: CountriesPresenterProtocol) {
        let tabBarVC = UITabBarController()
        
        let vc1 = UINavigationController(rootViewController: FirstViewController(presenter: presenter))
        vc1.navigationBar.prefersLargeTitles = true
        vc1.navigationBar.topItem?.title = "Countries"
        vc1.navigationItem.largeTitleDisplayMode = .automatic
        vc1.title = "Countries"
        
        let vc2 = UINavigationController(rootViewController: SecondViewController())
        vc2.navigationBar.prefersLargeTitles = true
        vc2.navigationBar.topItem?.title = "BLE"
        vc2.navigationItem.largeTitleDisplayMode = .automatic
        vc2.title = "BLE"
        
        tabBarVC.setViewControllers([vc1, vc2], animated: true)
        
        guard let items = tabBarVC.tabBar.items else { return }
        items[0].image = UIImage(systemName: "star.fill")
        items[1].image = UIImage(systemName: "star.fill")
        tabBarVC.modalPresentationStyle = .fullScreen
        baseController.present(tabBarVC, animated: true, completion: .none)
    }
    
    func showDetailCountrie(date: DetailCountry,basecontroller: UIViewController) {
        let vc = UINavigationController(rootViewController: DetailCountriesViewController(data: date.alpha2Code))
        vc.navigationBar.topItem?.title = date.name
        vc.modalPresentationStyle = .fullScreen
        basecontroller.present(vc, animated: true, completion: nil)
    }
}
