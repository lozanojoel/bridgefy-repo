//
//  ErrorPresenter.swift
//  Brdigefy
//
//  Created by Joel Lozano on 04/05/21.
//

import Foundation
import UIKit

protocol ErrorPresenterProtocol {
    func showErrorMessage(message: String, baseController: UIViewController)
}

class ErrorPresenter: ErrorPresenterProtocol {
    
    var wireframe: ErrorWireframeProtocol
    
    init() {
        self.wireframe = ErrorWireframe()
    }
    func showErrorMessage(message: String, baseController: UIViewController) {
        wireframe.showError(message: message, baseController: baseController, presenter: self)
    }
    
    
}
