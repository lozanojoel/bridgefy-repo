//
//  ErrorWireframe.swift
//  Brdigefy
//
//  Created by Joel Lozano on 05/05/21.
//

import Foundation
import UIKit

protocol ErrorWireframeProtocol {
    func showError(message: String, baseController: UIViewController, presenter: ErrorPresenterProtocol)
}

class ErrorWireframe: ErrorWireframeProtocol {
    
    func showError(message: String, baseController: UIViewController, presenter: ErrorPresenterProtocol) {
        let vc = UINavigationController(rootViewController: ErrorViewController(message: message,presenter: presenter))
        vc.hidesBarsOnTap = true
        vc.modalPresentationStyle = .popover
        vc.tabBarController?.tabBar.barTintColor = UIColor.clear
        baseController.present(vc, animated: true, completion: nil)
    }
    
}
