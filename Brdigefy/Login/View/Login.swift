//
//  Login.swift
//  Brdigefy
//
//  Created by Joel Lozano on 01/05/21.
//

import UIKit
import Foundation

class Login: UIViewController {
    
    var module: LoginModule?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.module = LoginModule(baseController: self)
    }
    @IBOutlet weak var buttonOutlet: UIButton!
    
    @IBOutlet weak var icon: UIImageView!{
        didSet{
            icon.image = icon.image?.withRenderingMode(.alwaysTemplate)
            icon.tintColor = UIColor.orange
        }
    }
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBAction func actionButton(_ sender: Any) {
        guard let mail = email.text, let pass = password.text else { return }
        module?.login(email: mail, password: pass)
    }
}
