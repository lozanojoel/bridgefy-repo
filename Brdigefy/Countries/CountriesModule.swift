//
//  CountriesModule.swift
//  Brdigefy
//
//  Created by Joel Lozano on 02/05/21.
//

import Foundation
import UIKit

class CountriesModule {
    
    let baseController: UIViewController
    var presenter: CountriesPresenterProtocol
    
    init(baseController: UIViewController) {
        self.baseController = baseController
        self.presenter = CountriesPresenter(baseController: baseController)
    }
    
    func showTabBar() {
        presenter.showTabBar()
    }
}
