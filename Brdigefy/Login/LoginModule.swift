//
//  LoginModule.swift
//  Brdigefy
//
//  Created by Joel Lozano on 01/05/21.
//

import Foundation
import UIKit

class LoginModule {
    
    var presenter: LoginPresenterProtocol
    
    init(baseController: UIViewController = UIViewController()) {
        self.presenter = LoginPresenter(baseController: baseController)
    }
    
    func login(email: String, password: String) {
        presenter.login(email: email, password: password)
    }
}
