//
//  DetailCountriesViewController.swift
//  Brdigefy
//
//  Created by Joel Lozano on 03/05/21.
//

import UIKit
import Foundation

class DetailCountriesViewController: UIViewController {

    @IBOutlet weak var flag: UIImageView!
    @IBOutlet weak var countrieName: UILabel!
    @IBOutlet weak var countriePlaceHolder: UILabel!
    
    @IBOutlet weak var viewMap: UIView! {
        didSet {
            viewMap.layer.cornerRadius = 10
            viewMap.layer.shadowColor = UIColor.black.cgColor
            viewMap.layer.shadowOffset = CGSize(width: 3, height: 3)
            viewMap.layer.shadowOpacity = 0.7
            viewMap.layer.shadowRadius = 4.0
        }
    }
    @IBOutlet weak var map: UIImageView! {
        didSet {
            map.tintColor = UIColor.orange
        }
    }
    @IBOutlet weak var descriptionMap: UILabel!
    @IBOutlet weak var kilometres: UILabel!
    @IBOutlet weak var position: UILabel!
    
    @IBOutlet weak var viewContact: UIView! {
        didSet {
            viewContact.layer.cornerRadius = 10
            viewContact.layer.shadowColor = UIColor.black.cgColor
            viewContact.layer.shadowOffset = CGSize(width: 3, height: 3)
            viewContact.layer.shadowOpacity = 0.7
            viewContact.layer.shadowRadius = 4.0
        }

    }
    @IBOutlet weak var people: UIImageView! {
        didSet {
            people.image = UIImage(named: "Population")?.withRenderingMode(.alwaysTemplate)
            people.tintColor = UIColor.orange
        }
    }
    @IBOutlet weak var lenguajeIcon: UIImageView! {
        didSet {
            lenguajeIcon.image = UIImage(named: "Languages")?.withRenderingMode(.alwaysTemplate)
            lenguajeIcon.tintColor = UIColor.orange
        }
    }
    @IBOutlet weak var telIcon: UIImageView! {
        didSet {
            telIcon.image = UIImage(named: "PhoneCode")?.withRenderingMode(.alwaysTemplate)
            telIcon.tintColor = UIColor.orange
        }
    }
    @IBOutlet weak var peopleLabel: UILabel!
    @IBOutlet weak var lenguajeLabel: UILabel!
    @IBOutlet weak var telLabel: UILabel!
    
    @IBOutlet weak var viewTime: UIView! {
        didSet {
            viewTime.layer.cornerRadius = 10
            viewTime.layer.shadowColor = UIColor.black.cgColor
            viewTime.layer.shadowOffset = CGSize(width: 3, height: 3)
            viewTime.layer.shadowOpacity = 0.7
            viewTime.layer.shadowRadius = 4.0
        }
    }
    @IBOutlet weak var timeIcon: UIImageView! {
        didSet {
            timeIcon.image = UIImage(named: "Timezones")?.withRenderingMode(.alwaysTemplate)
            timeIcon.tintColor = UIColor.orange
        }
    }
    @IBOutlet weak var time1: UILabel!
    @IBOutlet weak var time2: UILabel! {
        didSet {
            time2.text = "--"
        }
    }
    @IBOutlet weak var time3: UILabel! {
        didSet {
            time3.text = "--"
        }
    }
    
    @IBOutlet weak var viewCurrency: UIView! {
        didSet {
            viewCurrency.layer.cornerRadius = 10
            viewCurrency.layer.shadowColor = UIColor.black.cgColor
            viewCurrency.layer.shadowOffset = CGSize(width: 3, height: 3)
            viewCurrency.layer.shadowOpacity = 0.7
            viewCurrency.layer.shadowRadius = 4.0
        }
    }
    @IBOutlet weak var currencyIcon: UIImageView! {
        didSet {
            currencyIcon.image = UIImage(named: "Currency")?.withRenderingMode(.alwaysTemplate)
            currencyIcon.tintColor = UIColor.orange
        }
    }
    @IBOutlet weak var currency: UILabel!
    
    @IBOutlet weak var viewBordes: UIView! {
        didSet {
            viewBordes.layer.cornerRadius = 10
            viewBordes.layer.shadowColor = UIColor.black.cgColor
            viewBordes.layer.shadowOffset = CGSize(width: 3, height: 3)
            viewBordes.layer.shadowOpacity = 0.7
            viewBordes.layer.shadowRadius = 4.0
        }
    }
    @IBOutlet weak var flag1: UIImageView!
    @IBOutlet weak var borders: UILabel! {
        didSet {
            borders.text = "Borders"
        }
    }
    @IBOutlet weak var flagLabel: UILabel! {
        didSet {
            flagLabel.text = ""
        }
    }
    @IBOutlet weak var flag2: UIImageView!
    @IBOutlet weak var flag2Label: UILabel! {
        didSet {
            flag2Label.text = ""
        }
    }
    @IBOutlet weak var flag3: UIImageView!
    @IBOutlet weak var flag3Label: UILabel! {
        didSet {
            flag3Label.text = ""
        }
    }
    
    var data: String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(addTapped))
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: makeBackButton())
        countrieName.text = data
        getDetailCountrieSelected(countriCode: data)
    }
    
    func makeBackButton() -> UIButton {
            let backButtonImage = UIImage(systemName: "arrow.left")?.withRenderingMode(.alwaysTemplate)
            let backButton = UIButton(type: .custom)
            backButton.setImage(backButtonImage, for: .normal)
            backButton.tintColor = .orange
            backButton.setTitle("  Back", for: .normal)
            backButton.setTitleColor(.orange, for: .normal)
            backButton.addTarget(self, action: #selector(self.backButtonPressed), for: .touchUpInside)
            return backButton
        }

        @objc func backButtonPressed() {
            dismiss(animated: true, completion: nil)
        }
    
    @objc private func addTapped() {
        
    }
    
    
    init(data: String) {
        self.data = data
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getDetailCountrieSelected(countriCode: String) {
        let countriesRequest = ContriesDetailRequest(countriCode: countriCode)
        countriesRequest.getDetailCountrie { [weak self] result in
            switch result {
            case .failure( let error):
                print(error)
            case .success(let countries):
                DispatchQueue.main.async {
                    self?.countriePlaceHolder.text = countries.capital
                    self?.loadFlagImage(countrieFlag: countries.alpha2Code ?? "")
                    self?.descriptionMap.text = countries.subregion
                    self?.kilometres.text = String(countries.area)
                    self?.position.text = String(format: "", countries.latlng[0] ?? "") + String(format: "", countries.latlng[1] ?? "")
                    self?.map.image = UIImage(named: countries.alpha3Code ?? "")?.withRenderingMode(.alwaysTemplate)
                    self?.map.tintColor = UIColor.orange
                    self?.currency.text = countries.currencies[0]
                    self?.peopleLabel.text = String(countries.population)
                    self?.lenguajeLabel.text = countries.languages?[0]
                    self?.telLabel.text = String(countries.numericCode)
                    self?.time1.text = countries.timezones[0]
//                    self?.time2.text = countries.timezones[1]
//                    self?.time3.text = countries.timezones[2]
                    self?.flag1.image = self?.flag(country: countries.borders[0]).image()
//                    self?.flag2.image = self?.flag(country: countries.borders[1]).image()
//                    self?.flag3.image = self?.flag(country: countries.borders[2]).image()
                    self?.flagLabel.text = countries.borders[0]
//                    self?.flag2Label.text = countries.borders[1]
//                    self?.flag3Label.text = countries.borders[2]
                }
            }
        }
    }
    
    func flag(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
    func loadFlagImage(countrieFlag : String){
        if let url = URL(string: "https://flagpedia.net/data/flags/w1160/"+countrieFlag.lowercased()+".png") {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async { /// execute on main thread
                    self.flag.image = UIImage(data: data)
                }
            }
            
            task.resume()
        }
    }
}



