//
//  FirstViewController.swift
//  Brdigefy
//
//  Created by Joel Lozano on 02/05/21.
//

import UIKit

class FirstViewController: UIViewController, UISearchResultsUpdating{
    
    var data: [DetailCountry] = []
    var flagGroup: Bool = false
    var pila: [DetailCountry] = []
    var pila1: [DetailCountry] = []
    var pila2: [DetailCountry] = []
    var pila3: [DetailCountry] = []
    var pila4: [DetailCountry] = []
    var pila5: [DetailCountry] = []
    
    var presenter: CountriesPresenterProtocol
    var dataFilter: [DetailCountry] = []
    var searchController : UISearchController!
    var resultsController = UITableViewController()

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Group", style: .plain, target: self, action: #selector(addTapped))
        tableView?.dataSource = self;
        tableView?.delegate = self;
        configurateTableview()
        tableSettings()
        DispatchQueue.main.async {
            let a = APIManager.API.getData()
            self.data = a
            self.tableView.reloadData()
        }
        self.creatingSearhBar()
    }
    
    init(presenter: CountriesPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func addTapped() {
        if (flagGroup) {
            navigationItem.rightBarButtonItem?.title = "Group"
            self.searchController.searchBar.isHidden = false
            flagGroup = !flagGroup
            tableView.reloadData()
        } else {
            navigationItem.rightBarButtonItem?.title = "Ungroup"
            self.searchController.searchBar.isHidden = true
            flagGroup = !flagGroup
            tableView.reloadData()
        }
    }
    
    func creatingSearhBar() {
       self.searchController = UISearchController(searchResultsController: self.resultsController)
       tableView.tableHeaderView = self.searchController.searchBar
       self.searchController.searchResultsUpdater = self
     }

    
    private func configurateTableview(){
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UINib(nibName: "CellTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCell")
    }
    
    private func reloadTable(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func flag(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
     
        self.dataFilter = self.data.filter { (countrie: DetailCountry) -> Bool in
            if countrie.alpha2Code.lowercased().contains(self.searchController.searchBar.text!.lowercased()){
                 return true
              } else{
                 return false
              }
           }
        self.resultsController.tableView.reloadData()
       }
    
    func tableSettings() {
       self.resultsController.tableView.dataSource = self
       self.resultsController.tableView.delegate = self
       self.resultsController.tableView.rowHeight = 204
       self.resultsController.tableView.separatorStyle = .none
     }
}

extension FirstViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (flagGroup){
            return pila.count
        } else {
            if tableView == self.tableView {
                  return data.count
               }
               else {
                  return dataFilter.count
               }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! CellTableViewCell
        if (flagGroup){
            switch indexPath.section {
            case 0:
                cell.countrieLabel.text = pila[indexPath.row].name
                cell.acronym.text = pila[indexPath.row].alpha2Code + " / " + pila[indexPath.row].alpha3Code
                cell.flag.image = flag(country: pila[indexPath.row].alpha2Code).image()
                return cell
            case 1:
                cell.countrieLabel.text = pila1[indexPath.row].name
                cell.acronym.text = pila1[indexPath.row].alpha2Code + " / " + pila1[indexPath.row].alpha3Code
                cell.flag.image = flag(country: pila1[indexPath.row].alpha2Code).image()
                return cell
            case 2:
                cell.countrieLabel.text = pila2[indexPath.row].name
                cell.acronym.text = pila2[indexPath.row].alpha2Code + " / " + pila2[indexPath.row].alpha3Code
                cell.flag.image = flag(country: pila2[indexPath.row].alpha2Code).image()
                return cell
            case 3:
                cell.countrieLabel.text = pila3[indexPath.row].name
                cell.acronym.text = pila3[indexPath.row].alpha2Code + " / " + pila3[indexPath.row].alpha3Code
                cell.flag.image = flag(country: pila3[indexPath.row].alpha2Code).image()
                return cell
            case 4:
                cell.countrieLabel.text = pila4[indexPath.row].name
                cell.acronym.text = pila4[indexPath.row].alpha2Code + " / " + pila4[indexPath.row].alpha3Code
                cell.flag.image = flag(country: pila4[indexPath.row].alpha2Code).image()
                return cell
            case 5:
                cell.countrieLabel.text = pila5[indexPath.row].name
                cell.acronym.text = pila5[indexPath.row].alpha2Code + " / " + pila5[indexPath.row].alpha3Code
                cell.flag.image = flag(country: pila5[indexPath.row].alpha2Code).image()
                return cell
            default:
                cell.countrieLabel.text = data[indexPath.row].name
                cell.acronym.text = data[indexPath.row].alpha2Code + " / " + data[indexPath.row].alpha3Code
                cell.flag.image = flag(country: data[indexPath.row].alpha2Code).image()
                return cell
            }
        } else {
            let pais: DetailCountry
            if tableView == self.tableView {
                pais = data[indexPath.row]
            } else {
                pais = dataFilter[indexPath.row]
            }
            cell.countrieLabel.text = pais.name
            cell.acronym.text = pais.alpha2Code + " / " + pais.alpha3Code
            cell.flag.image = flag(country: pais.alpha2Code).image()
            
            return cell
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(70)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if(flagGroup) {
            switch section {
            case 0:
                for item in data.filter({ $0.region == "Asia" }) {
                    pila.append(item)
                }
                return pila[section].region
            case 1:
                for item in data.filter({ $0.region == "Americas" }) {
                    pila1.append(item)
                }
                return pila1[section].region
            case 2:
                for item in data.filter({ $0.region == "Europe" }) {
                    pila2.append(item)
                }
                return pila2[section].region
            case 3:
                for item in data.filter({ $0.region == "Oceania" }) {
                    pila3.append(item)
                }
                return pila3[section].region
                
            case 4:
                for item in data.filter({ $0.region == "Africa" }) {
                    pila4.append(item)
                }
                return pila4[section].region
            case 5:
                for item in data.filter({ $0.region == "Polar" }) {
                    pila5.append(item)
                }
                return pila5[section].region
            default:
                return "Europe"
            }
        } else {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let index = tableView.indexPathForSelectedRow;
        let sectionNumber = index?.section
        if (flagGroup){
            switch sectionNumber {
            case 0:
                self.presenter.showDetailCountrie(data: self.pila[indexPath.row], basecontroller: self)
            case 1:
                self.presenter.showDetailCountrie(data: self.pila1[indexPath.row], basecontroller: self)
            case 2:
                self.presenter.showDetailCountrie(data: self.pila2[indexPath.row], basecontroller: self)
            case 3:
                self.presenter.showDetailCountrie(data: self.pila3[indexPath.row], basecontroller: self)
            case 4:
                self.presenter.showDetailCountrie(data: self.pila4[indexPath.row], basecontroller: self)
            case 5:
                self.presenter.showDetailCountrie(data: self.pila5[indexPath.row], basecontroller: self)
            default:
                return
            }
        } else {
            if (tableView == self.tableView){
                self.presenter.showDetailCountrie(data: self.data[indexPath.row], basecontroller: self)
            } else {
                self.presenter.showDetailCountrie(data: self.dataFilter[indexPath.row], basecontroller: self)
            }
            
        }
    }
}


extension String {
    func image() -> UIImage? {
        let size = CGSize(width: 40, height: 40)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.white.set()
        let rect = CGRect(origin: .zero, size: size)
        UIRectFill(CGRect(origin: .zero, size: size))
        (self as AnyObject).draw(in: rect, withAttributes: [.font: UIFont.systemFont(ofSize: 40)])
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}


