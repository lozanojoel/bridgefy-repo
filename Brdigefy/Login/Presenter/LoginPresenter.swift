//
//  LoginPresenter.swift
//  Brdigefy
//
//  Created by Joel Lozano on 01/05/21.
//

import Foundation
import UIKit

protocol LoginPresenterProtocol: class {
    func login(email: String, password: String)
}

class LoginPresenter: LoginPresenterProtocol {
    
    var email: String?
    var password: String?
    var wireframe: LoginWireframeProtocol
    var interactor: LoginInteractorProtocol
    var baseController: UIViewController
    
    init(baseController: UIViewController) {
        self.baseController = baseController
        self.wireframe = LoginWireframe()
        self.interactor = LoginInteractor()
    }
    
    func login(email: String, password: String) {
        let result = interactor.login(email: email, password: password)
        switch result {
        case .success:
            wireframe.showTableView(baseController: baseController)
        case .fail:
            wireframe.showError()
        }
    }
}
