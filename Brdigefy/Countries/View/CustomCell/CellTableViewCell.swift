//
//  CellTableViewCell.swift
//  Brdigefy
//
//  Created by Joel Lozano on 02/05/21.
//

import UIKit

class CellTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var flag: UIImageView!
    @IBOutlet weak var countrieLabel: UILabel!
    @IBOutlet weak var acronym: UILabel!
    @IBOutlet weak var rigthArrow: UIImageView! {
        didSet {
            rigthArrow.image = UIImage(systemName: "arrow.right")
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
}
