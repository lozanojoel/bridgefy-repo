//
//  APIManager.swift
//  Brdigefy
//
//  Created by Joel Lozano on 01/05/21.
//

import Foundation

class APIManager: NSObject {
    static let API = APIManager()
    var countriesData = [DetailCountry]()
    var countriesDetailData: DetailViewCountrie?
    
    func getCountriesData(){
        let countriesRequest = ContriesRequest()
        countriesRequest.getCountries { [weak self] result in
            switch result {
            case .failure( let error):
                print(error)
            case .success(let countries):
                self?.countriesData = countries
            }
        }
    }
    
    func getData() -> [DetailCountry] {
        return countriesData
    }
    
    func getDetailCountrieSelected(countriCode: String) {
        let countriesRequest = ContriesDetailRequest(countriCode: countriCode)
        countriesRequest.getDetailCountrie { [weak self] result in
            print("1")
            switch result {
            case .failure( let error):
                print(error)
            case .success(let countries):
                self?.countriesDetailData = countries
            }
        }
    }
    
    func getDetailCountrie() -> DetailViewCountrie {
        return countriesDetailData!
    }
    
    func loginValidation() -> [String: Any]{
        let user = LoginManager()
        return user.getUserValidation()
    }
}
