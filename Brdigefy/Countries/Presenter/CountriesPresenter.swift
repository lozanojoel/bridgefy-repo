//
//  CountriesPresenter.swift
//  Brdigefy
//
//  Created by Joel Lozano on 02/05/21.
//

import Foundation
import UIKit

protocol CountriesPresenterProtocol {
    func showTabBar()
    func showDetailCountrie(data :DetailCountry, basecontroller: UIViewController)
}

class CountriesPresenter: CountriesPresenterProtocol {
    
    let wireframe: CountriesWireframeProtocol
    let interactor: CountriesInteractorProtocol
    let baseController: UIViewController
    
    init(baseController: UIViewController ) {
        self.baseController = baseController
        self.wireframe = CountriesWireframe(baseController: baseController)
        self.interactor = CountriesInteractor()
    }
    
    func showTabBar() {
        loadDataCountries()
        wireframe.showTabBar(baseController: baseController, presenter: self)
    }
    
    func loadDataCountries(){
        APIManager.API.getCountriesData()
    }
    
    func showDetailCountrie(data: DetailCountry, basecontroller: UIViewController) {
        wireframe.showDetailCountrie(date: data, basecontroller: basecontroller)
    }
}
