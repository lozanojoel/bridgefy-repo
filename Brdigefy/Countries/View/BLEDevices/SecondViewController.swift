//
//  SecondViewController.swift
//  Brdigefy
//
//  Created by Joel Lozano on 02/05/21.
//

import UIKit
import CoreBluetooth


struct DeviceInformation {
    var identifier: String
    var name: String
    var state: String
}


class SecondViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let GenericAccess = CBUUID(string: "0x1800")
    var cbCentralManager: CBCentralManager!
    var devices: [DeviceInformation] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Scan", style: .plain, target: self, action: #selector(addTapped))
        cbCentralManager = CBCentralManager(delegate: self, queue: nil)
        configurateTableview()
    }
    
    @objc private func addTapped() {
        
    }
    
    private func configurateTableview(){
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UINib(nibName: "BLETableViewCell", bundle: nil), forCellReuseIdentifier: "BLECell")
    }

}
extension SecondViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
           central.scanForPeripherals(withServices: nil, options: nil)
           print("Scanning...")
          }

    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
                        advertisementData: [String: Any], rssi RSSI: NSNumber) {
        
        let identifier = "\(peripheral.identifier)"
        let name = "\(peripheral.name ?? "No Name")"
        let state = "\(peripheral.state)"
        let newDevice = DeviceInformation(identifier: identifier, name: name, state: state)
        self.devices.append(newDevice)
        tableView.reloadData()
    }
}


extension SecondViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BLECell", for: indexPath) as! BLETableViewCell
        cell.name.text = devices[indexPath.row].name
        cell.identifier.text = devices[indexPath.row].identifier
        cell.status.text = devices[indexPath.row].state
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(88)
    }


}
